# Photolar #

Фото приложение с поиском по сожержимому при помощи TensorFlow

### Технологии и библиотеки ###

* Clean Architecture
* MVP
* RxJava
* StoreIO
* Tests(JUnit, Robolectric, Mockito)
* TensorFlow

### In-process features ###
*Подгрузка фотографий с локального устройства (feature_photos_list branch)