package com.gilazov.data.db;

import com.gilazov.data.common.CacheManager;
import com.gilazov.data.db.table.PhotosTable;
import com.gilazov.data.entity.PhotoDAO;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 25.10.16.
 */

public class PhotoCacheManager implements CacheManager<PhotoDAO>{

    private StorIOSQLite db;

    public PhotoCacheManager(StorIOSQLite db) {
        this.db = db;
    }

    public void save(List<PhotoDAO> photoDAOs) {
        db
                .put()
                .objects(photoDAOs)
                .prepare()
                .executeAsBlocking();
    }

    public void save(PhotoDAO photoDAO) {
        db
                .put()
                .object(photoDAO)
                .prepare()
                .executeAsBlocking();
    }

    public Observable<PutResult> save2(PhotoDAO photoDAO) {
        return db
                .put()
                .object(photoDAO)
                .prepare()
                .asRxObservable();
    }

    public void delete(List<PhotoDAO> photoDAOs) {
        db
                .delete()
                .objects(photoDAOs)
                .prepare()
                .executeAsBlocking();
    }

    public void delete(PhotoDAO photoDAO) {
        db
                .delete()
                .object(photoDAO)
                .prepare()
                .executeAsBlocking();
    }

    public Observable<List<PhotoDAO>> getAll() {
        return db
                .get()
                .listOfObjects(PhotoDAO.class)
                .withQuery(Query.builder()
                        .table(PhotosTable.TABLE)
                        .build())
                .prepare()
                .asRxObservable();
    }

    @Override
    public void clear() {
        db
                .executeSQL()
                .withQuery(RawQuery.builder()
                            .query("DELETE FROM " + PhotosTable.TABLE)
                            .build())
                .prepare()
                .executeAsBlocking();
    }

}
