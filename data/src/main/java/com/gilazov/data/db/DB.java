package com.gilazov.data.db;

import android.content.Context;

import com.gilazov.data.entity.PhotoDAO;
import com.gilazov.data.entity.PhotoDAOStorIOSQLiteDeleteResolver;
import com.gilazov.data.entity.PhotoDAOStorIOSQLiteGetResolver;
import com.gilazov.data.entity.PhotoDAOStorIOSQLitePutResolver;
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

/**
 * Created by r on 24.10.16.
 */

public class DB {

    private static StorIOSQLite DB;

    public static StorIOSQLite getInstance(Context context) {

        if (DB == null) {
            DB =  DefaultStorIOSQLite.builder()
                    .sqliteOpenHelper(new DBOpenHelper(context))
                    .addTypeMapping(PhotoDAO.class, SQLiteTypeMapping.<PhotoDAO>builder()
                            .putResolver(new PhotoDAOStorIOSQLitePutResolver()) // object that knows how to perform Put Operation (insert or update)
                            .getResolver(new PhotoDAOStorIOSQLiteGetResolver()) // object that knows how to perform Get Operation
                            .deleteResolver(new PhotoDAOStorIOSQLiteDeleteResolver())  // object that knows how to perform Delete Operation
                            .build())
                    .build();
        }

        return DB;
    }

}
