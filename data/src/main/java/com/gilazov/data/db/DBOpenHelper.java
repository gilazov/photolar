package com.gilazov.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gilazov.data.db.table.PhotosTable;

/**
 * Created by r on 24.10.16.
 */

public class DBOpenHelper extends SQLiteOpenHelper{

    public DBOpenHelper(Context context){
        super(context, "photolar_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PhotosTable.getCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
