package com.gilazov.data.entity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.gilazov.data.db.table.PhotosTable;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

/**
 * Created by r on 24.10.16.
 */
//todo add equals strategy (for sinc with local storage) maybe byID maybe by fields

@StorIOSQLiteType(table = PhotosTable.TABLE)
public class PhotoDAO {

    @Nullable
    @StorIOSQLiteColumn(name = PhotosTable.COLUMN_ID, key = true)
    Long id;

    @NonNull
    @StorIOSQLiteColumn(name = PhotosTable.COLUMN_NAME)
    String name;

    @NonNull
    @StorIOSQLiteColumn(name = PhotosTable.COLUMN_URL)
    String url;

    public PhotoDAO() {
    }

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getUrl() {
        return url;
    }

    public void setUrl(@NonNull String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhotoDAO photoDAO = (PhotoDAO) o;

        if (!name.equals(photoDAO.name)) return false;
        return url.equals(photoDAO.url);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }
}
