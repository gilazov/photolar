package com.gilazov.data.repository;

import com.gilazov.data.entity.PhotoDAO;
import com.gilazov.data.source.DevicePhotoProvider;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 24.10.16.
 *
 */

public class PhotosRepository {

   // private CacheManager<PhotoDAO> cache;

    private DevicePhotoProvider devicePhotoProvider;

    public PhotosRepository(DevicePhotoProvider devicePhotoProvider) {
        this.devicePhotoProvider = devicePhotoProvider;
    }

    protected Observable<List<PhotoDAO>> getDataFromPhotoProvider() {
       return devicePhotoProvider.getPhotos();
    }

    /*protected Observable<List<PhotoDAO>> getDataFromCache() {
        return cache.getAll();
    }*/

    public Observable<List<PhotoDAO>> getAll(){
        //// FIXME: add caching logic
        return getDataFromPhotoProvider();
    }

}
