package com.gilazov.data.source;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.MediaStore;

import com.gilazov.data.entity.PhotoDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;

/**
 * Created by r on 24.10.16.
 */

public class DevicePhotoProvider {

    private ContentResolver contentResolver;

    public DevicePhotoProvider(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    private final String[] columns = {
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DISPLAY_NAME };

    private final String orderBy = MediaStore.Images.Media._ID;

    public Observable<List<PhotoDAO>> getPhotos(){

        return Observable.fromCallable(new Callable<List<PhotoDAO>>() {
                                           @Override
                                           public List<PhotoDAO> call() throws Exception {

                                               List<PhotoDAO> photos = new ArrayList<>();

                                               Cursor cursor = contentResolver.query(
                                                       MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                                       columns,
                                                       null,
                                                       null,
                                                       orderBy);

                                               if (cursor != null) {

                                                   //Total number of images
                                                   int count = cursor.getCount();

                                                   for (int i = 0; i < count; i++) {
                                                       cursor.moveToPosition(i);

                                                       int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                                                       int nameColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);

                                                       PhotoDAO photoDAO = new PhotoDAO();
                                                       photoDAO.setName(cursor.getString(nameColumnIndex));
                                                       photoDAO.setUrl(cursor.getString(dataColumnIndex));

                                                       photos.add(photoDAO);

                                                   }



                                               }
                                               return photos;
                                           }
                                       }

        );
    }

}
