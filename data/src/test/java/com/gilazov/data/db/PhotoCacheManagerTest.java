package com.gilazov.data.db;

import com.gilazov.data.entity.PhotoDAO;
import com.pushtorefresh.storio.sqlite.BuildConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by r on 27.10.16.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class PhotoCacheManagerTest {

    @Test
    public void saveIfNotExists() throws Exception {

        final PhotoCacheManager cacheManager = new PhotoCacheManager(DB.getInstance(RuntimeEnvironment.application));

        PhotoDAO expectedPhoto = new PhotoDAO();
        expectedPhoto.setId(1l);
        expectedPhoto.setName("name");
        expectedPhoto.setUrl("url");

        cacheManager.save(expectedPhoto);
        int expectedSize = 1;

        int size = cacheManager.getAll().toBlocking().first().size();

        assertThat(size).isEqualTo(expectedSize);

    }

    @Test
    public void saveIfExist() throws Exception{
        PhotoCacheManager cacheManager = new PhotoCacheManager(DB.getInstance(RuntimeEnvironment.application));

        PhotoDAO expectedPhoto = new PhotoDAO();
        expectedPhoto.setId(1l);
        expectedPhoto.setName("name2");
        expectedPhoto.setUrl("url2");

        cacheManager.save(expectedPhoto);
        cacheManager.save(expectedPhoto);

        int expectedSize = 1;

        int size = cacheManager.getAll().toBlocking().first().size();
        assertThat(size).isEqualTo(expectedSize);

    }

    @Test
    public void delete() throws Exception{

        PhotoCacheManager cacheManager = new PhotoCacheManager(DB.getInstance(RuntimeEnvironment.application));

        PhotoDAO photo1 = new PhotoDAO();
        photo1.setId(1l);
        photo1.setName("name");
        photo1.setUrl("url");

        PhotoDAO photo2 = new PhotoDAO();
        photo2.setId(2l);
        photo2.setName("name2");
        photo2.setUrl("url2");

        cacheManager.save(photo1);
        cacheManager.save(photo2);
        cacheManager.delete(photo1);

        PhotoDAO expectedPhoto = photo2;
        int expectedSize = 1;

        int size = cacheManager.getAll().toBlocking().first().size();

        assertThat(size).isEqualTo(expectedSize);

        PhotoDAO actualPhoto = cacheManager.getAll().toBlocking().first().get(0);

        assertThat(actualPhoto).isEqualTo(expectedPhoto);

    }

    @Test
   public void clear(){

        PhotoCacheManager cacheManager = new PhotoCacheManager(DB.getInstance(RuntimeEnvironment.application));

        PhotoDAO photo1 = new PhotoDAO();
        photo1.setId(1l);
        photo1.setName("name");
        photo1.setUrl("url");

        PhotoDAO photo2 = new PhotoDAO();
        photo2.setId(2l);
        photo2.setName("name2");
        photo2.setUrl("url2");

        cacheManager.save(photo1);
        cacheManager.save(photo2);

        cacheManager.clear();

        assertThat(cacheManager.getAll().toBlocking().first()).hasSize(0);
    }

}