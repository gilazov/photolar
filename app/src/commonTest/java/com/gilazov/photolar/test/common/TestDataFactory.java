package com.gilazov.photolar.test.common;

import com.gilazov.domain.model.Photo;

import java.util.ArrayList;
import java.util.List;

public class TestDataFactory {

    public static List<Photo> makeListPhotos(int count){
        List<Photo> list = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            list.add(makePhoto(i));
        }
        return list;
    }

    public static Photo makePhoto(int id){
        Photo photo = new Photo();
        photo.setName("name "+ id);
        photo.setId(id);
        photo.setUrl("url/"+ id);
        return photo;
    }

}
