package com.gilazov.photolar.base.adapter.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
