package com.gilazov.photolar.base.view;

import android.content.Context;

import java.util.List;

/**
 * Created by r on 07.11.16.
 */

public interface MVPRecyclerFragmentView<T> {

    void setItems (List<T> list);
    void showProgress();
    void hideProgress();
    void showEmptyView();
    void hideEmptyView();
    void showErrorView();
    void hideErrorView();

    Context getContext();

}
