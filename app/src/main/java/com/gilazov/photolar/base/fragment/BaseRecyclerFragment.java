package com.gilazov.photolar.base.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gilazov.photolar.R;
import com.gilazov.photolar.base.adapter.BaseRecyclerAdapter;
import com.gilazov.photolar.base.adapter.listener.OnItemClickListener;
import com.gilazov.photolar.base.adapter.listener.OnItemLongClickListener;
import com.gilazov.photolar.base.adapter.viewholder.BaseViewHolder;
import com.gilazov.photolar.base.recycler.DividerItemDecoration;

public abstract class BaseRecyclerFragment<T,VH extends BaseViewHolder> extends Fragment{

    final private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            onRecyclerViewItemClick(view, position);
        }
    };

    final private OnItemLongClickListener mOnItemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(View view, int position) {
            return onRecyclerViewItemLongClick(view, position);
        }
    };

    private RecyclerView mRecyclerView;
    private BaseRecyclerAdapter<T,? extends VH> mAdapter;
    private RecyclerView.ItemDecoration mItemDecorator;

    private View mProgressBarHolder;
    private View mEmptyView;
    private View mErrorView;

    /**
     * Called on recyclerViews adapter item long click.
     *
     * @param view     - view, that've been clicked
     * @param position position of view in adapter
     * @return true, if handled.
     */

    protected boolean onRecyclerViewItemLongClick(View view, int position){
        return false;
    }

    /**
     * Called on recyclerViews adapter item click.
     *
     * @param view     - view, that've been clicked
     * @param position position of view in adapter
     */

    protected abstract void onRecyclerViewItemClick(View view, int position);
    /**
     * @return current fragment layout resId
     */
    protected int getLayoutId() {
        return R.layout.fragment_recycler;
    }

    protected RecyclerView getRecyclerView(){
        return mRecyclerView;
    }

    private void initView(View root) {
        if (root == null) {
            throw new IllegalStateException("Content view not yet created");
        }
        if (root instanceof RecyclerView) {
            mRecyclerView = (RecyclerView) root;
        } else {
            mRecyclerView = (RecyclerView) root.findViewById(R.id.recycler);
        }
        Context context = root.getContext();
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = getLayoutManager(context);
        mRecyclerView.setLayoutManager(layoutManager);

        RecyclerView.ItemAnimator itemAnimator = getItemAnimator();
        mRecyclerView.setItemAnimator(itemAnimator);

        if (isNeedDivider()) {
            if(mItemDecorator!=null){
                mRecyclerView.removeItemDecoration(mItemDecorator);
            }
            mItemDecorator = getItemDecorator(context, layoutManager);
            if (mItemDecorator!= null) {
                mRecyclerView.addItemDecoration(mItemDecorator);
            }
        }

        mProgressBarHolder = root.findViewById(R.id.progress_bar);

        mEmptyView = root.findViewById(R.id.emptyView);

        mErrorView = root.findViewById(R.id.errorView);

    }

    protected RecyclerView.ItemDecoration getItemDecorator(Context context, RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            return new DividerItemDecoration(context, ((LinearLayoutManager) layoutManager).getOrientation());
        }
        return null;
    }

    protected boolean isNeedDivider() {
        return false;
    }

    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new GridLayoutManager(context, 2);
    }

    protected BaseRecyclerAdapter<T,? extends VH> getAdapter() {
        return mAdapter;
    }

    protected void setAdapter(BaseRecyclerAdapter<T,? extends VH> adapter) {
        mAdapter = adapter;
        if (mRecyclerView != null&&mAdapter!=null) {
            adapter.setOnItemClickListener(mOnItemClickListener);
            adapter.setOnItemLongClickListener(mOnItemLongClickListener);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    protected void showProgress(boolean b){
        if(mProgressBarHolder!=null) {
            if (b){
                mProgressBarHolder.setVisibility(View.VISIBLE);
            } else {
                mProgressBarHolder.setVisibility(View.GONE);
            }
        }
    }

    protected void showEmptyView(boolean b ){
        if (mEmptyView != null) {
            if (b){
                mEmptyView.setVisibility(View.VISIBLE);
            }else {
                mEmptyView.setVisibility(View.GONE);
            }
        }
    }

    protected void showErrorView(boolean b ){
        if (mErrorView != null) {
            if (b){
                mErrorView.setVisibility(View.VISIBLE);
            }else {
                mErrorView.setVisibility(View.GONE);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        if(mAdapter!=null) {
            setAdapter(mAdapter);
        }
    }

    public RecyclerView.ItemAnimator getItemAnimator() {
        RecyclerView.ItemAnimator animator=new DefaultItemAnimator();
        animator.setAddDuration(100);
        animator.setRemoveDuration(50);
        return animator;
    }

}
