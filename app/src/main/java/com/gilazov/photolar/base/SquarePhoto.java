package com.gilazov.photolar.base;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by r on 08.11.16.
 */

public class SquarePhoto extends ImageView {

    public SquarePhoto(Context context) {
        super(context);
    }

    public SquarePhoto(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquarePhoto(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

}
