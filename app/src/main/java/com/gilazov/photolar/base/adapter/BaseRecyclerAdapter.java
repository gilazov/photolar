package com.gilazov.photolar.base.adapter;

import android.support.v7.widget.RecyclerView;

import com.gilazov.photolar.base.adapter.listener.OnItemClickListener;
import com.gilazov.photolar.base.adapter.listener.OnItemLongClickListener;
import com.gilazov.photolar.base.adapter.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public abstract class BaseRecyclerAdapter<T, R extends BaseViewHolder> extends RecyclerView.Adapter<R> {

    protected List<T> mData;
    protected List<Boolean> mSelected;
    protected OnItemClickListener mOnItemClickListener;
    protected OnItemLongClickListener mOnItemLongClickListener;

    public BaseRecyclerAdapter(List<T> data){
        initData(data);
    }

    private void initData(List<T> data){
        mData=data!=null?data:new ArrayList<T>();
        mSelected=new ArrayList<>(mData.size());
        for(int i=0,size=mData.size();i<size;i++){
            mSelected.add(false);
        }
    }

    public void setData(List<T> data) {
        initData(data);
        notifyDataSetChanged();
    }

    public void addItem(T item){
        int position=mData.size();
        addItem(position,item);
    }

    public void addItem(int position, T item){
        mData.add(position,item);
        mSelected.add(position,false);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mData.remove(position);
        mSelected.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItem(T item) {
        int position = mData.indexOf(item);
        if (position >= 0) {
            removeItem(position);
        }
    }

    public void removeItems(List<Integer> positions){
        Collections.sort(positions, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return rhs-lhs;
            }
        });
        while (!positions.isEmpty()) {
            if (positions.size() == 1) {
                removeItem(positions.get(0));
                positions.remove(0);
            } else {
                int count = 1;
                while (positions.size() > count && positions.get(count).equals(positions.get(count - 1) - 1)) {
                    ++count;
                }

                if (count == 1) {
                    removeItem(positions.get(0));
                } else {
                    removeItemRange(positions.get(count - 1), count);
                }

                for (int i = 0; i < count; ++i) {
                    positions.remove(0);
                }
            }
        }


    }

    public void removeItemRange(int positionStart, int itemCount){
        for(int i=0;i<itemCount;i++){
            mData.remove(positionStart);
            mSelected.remove(positionStart);
        }
        notifyItemRangeRemoved(positionStart,itemCount);
    }

    public void setOnItemClickListener(OnItemClickListener mListener) {
        this.mOnItemClickListener = mListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.mOnItemLongClickListener = onItemLongClickListener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public T getItem(int position) {
        return mData.get(position);
    }

    public List<T> getItems() {
        return mData;
    }

    public boolean isSelected(int position){
        return mSelected.get(position);
    }

    public void setSelection(int position, boolean isSelected){
        mSelected.set(position,isSelected);
        notifyItemChanged(position);
    }

    public void setAllSelection(boolean isSelected){
        for(int i=0,size=mSelected.size();i<size;i++){
            if(mSelected.get(i)!=isSelected){
                mSelected.set(i,isSelected);
                notifyItemChanged(i);
            }
        }
    }

    public List<T> getSelected() {
        List<T> selected=new ArrayList<>();
        for(int i=0,size=mData.size();i<size;i++){
            if(mSelected.get(i)){
                selected.add(mData.get(i));
            }
        }
        return selected;
    }

    public List<Integer> getSelectedPositions(){
        List<Integer> selectedPositions=new ArrayList<>();
        for(int i=0,size=mSelected.size();i<size;i++){
            if(mSelected.get(i)){
                selectedPositions.add(i);
            }
        }
        return selectedPositions;
    }

    public int getSelectedCount(){
        int count=0;
        for(Boolean selected:mSelected){
            if(selected){
                count++;
            }
        }
        return count;
    }

}

