package com.gilazov.photolar.base.adapter.listener;

import android.view.View;

public interface OnItemLongClickListener {
    boolean onItemLongClick(View view, int position);
}