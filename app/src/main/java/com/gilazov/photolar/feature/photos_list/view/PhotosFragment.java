package com.gilazov.photolar.feature.photos_list.view;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.gilazov.domain.model.Photo;
import com.gilazov.photolar.R;
import com.gilazov.photolar.base.fragment.BaseRecyclerFragment;
import com.gilazov.photolar.base.view.MVPRecyclerFragmentView;
import com.gilazov.photolar.feature.photos_list.adapter.PhotoAdapter;
import com.gilazov.photolar.feature.photos_list.presenter.PhotosFragmentPresenter;
import com.gilazov.photolar.feature.photos_list.viewholder.PhotoViewHolder;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by ruslan on 12.09.16.
 */
public class PhotosFragment extends BaseRecyclerFragment<Photo,PhotoViewHolder> implements MVPRecyclerFragmentView<Photo>{

    private PhotosFragmentPresenter presenter;

    @SuppressWarnings("unused")
    public static PhotosFragment newInstance() {
        PhotosFragment fragment = new PhotosFragment();
        return fragment;
    }

    @Override
    protected boolean isNeedDivider() {
        return true;
    }

    @Override
    protected void onRecyclerViewItemClick(View view, int position) {
        final Photo photo = getAdapter().getItem(position);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recycler;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Context context = view.getContext();

        presenter = new PhotosFragmentPresenter(this);

        RxPermissions.getInstance(context)
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        presenter.loadData();
                    } else {
                        Log.d("PERMISSION", "onViewCreated: permission denied");
                        showErrorView();
                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @Override
    public void setItems(List<Photo> list) {
        setAdapter(new PhotoAdapter(list));
    }

    @Override
    public void showProgress() {
        showProgress(true);
        hideEmptyView();
        hideErrorView();
    }

    @Override
    public void hideProgress() {
        showProgress(false);
    }

    @Override
    public void showEmptyView() {
        showEmptyView(true);
        hideProgress();
        hideErrorView();
    }

    @Override
    public void hideEmptyView() {
        showEmptyView(false);
    }

    @Override
    public void showErrorView() {
        showErrorView(true);
        hideProgress();
        hideEmptyView();
    }

    @Override
    public void hideErrorView() {
        showErrorView(false);
    }

}