package com.gilazov.photolar.feature.photos_list.viewholder;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gilazov.domain.model.Photo;
import com.gilazov.photolar.R;
import com.gilazov.photolar.base.adapter.listener.OnItemClickListener;
import com.gilazov.photolar.base.adapter.viewholder.BaseViewHolder;

import butterknife.BindView;

/**
 * Created by ruslan on 12.09.16.
 */

public class PhotoViewHolder extends BaseViewHolder {

    @BindView(R.id.picture)
    public ImageView picture;

    public PhotoViewHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView, clickListener);
    }

    public void init(Photo photo) {

        Glide
                .with(itemView.getContext())
                .load(photo.getUrl())
                .into(picture);

    }

}
