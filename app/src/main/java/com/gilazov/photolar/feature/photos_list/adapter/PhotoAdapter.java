package com.gilazov.photolar.feature.photos_list.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gilazov.domain.model.Photo;
import com.gilazov.photolar.R;
import com.gilazov.photolar.base.adapter.BaseRecyclerAdapter;
import com.gilazov.photolar.feature.photos_list.viewholder.PhotoViewHolder;

import java.util.List;

/**
 * Created by ruslan on 12.09.16.
 */
public class PhotoAdapter extends BaseRecyclerAdapter<Photo, PhotoViewHolder> {

    public PhotoAdapter(List<Photo> photos) {
            super(photos);
            }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.fragment_photos_item, parent, false);
            return new PhotoViewHolder(view, mOnItemClickListener);
            }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
            holder.init(getItem(position));
            }

}
