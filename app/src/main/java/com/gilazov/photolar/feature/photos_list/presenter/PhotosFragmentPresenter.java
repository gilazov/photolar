package com.gilazov.photolar.feature.photos_list.presenter;

import com.gilazov.data.repository.PhotosRepository;
import com.gilazov.data.source.DevicePhotoProvider;
import com.gilazov.domain.model.Photo;
import com.gilazov.domain.usecase.GetPhotosUseCase;
import com.gilazov.photolar.base.presenter.MVPRecyclerFragmentPresenter;
import com.gilazov.photolar.base.view.MVPRecyclerFragmentView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by r on 07.11.16.
 */

public class PhotosFragmentPresenter implements MVPRecyclerFragmentPresenter<Photo>{

    private CompositeSubscription mCompositeSubscription  = new CompositeSubscription();

    private MVPRecyclerFragmentView view;

    private GetPhotosUseCase getPhotosUseCase;

    public void setView(MVPRecyclerFragmentView view) {
        this.view = view;
    }

    public void setModel(GetPhotosUseCase getPhotosUseCase) {
        this.getPhotosUseCase = getPhotosUseCase;
    }

    public PhotosFragmentPresenter(MVPRecyclerFragmentView view) {
        setView(view);
        //todo fix with dagger 2
        getPhotosUseCase = new GetPhotosUseCase(new PhotosRepository(
                new DevicePhotoProvider(view.getContext().getContentResolver())));

        setModel(getPhotosUseCase);
    }

    public PhotosFragmentPresenter(MVPRecyclerFragmentView view, GetPhotosUseCase useCase) {
        setView(view);
        //todo fix with dagger 2
        setModel(useCase);
    }

    @Override
    public void loadData() {

        view.showProgress();//todo fix with rx
        mCompositeSubscription.add(
                getPhotosUseCase
                    .getPhotos()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(photos -> {
                        if (view != null) {
                            //todo fix with switchIfEmpty
                            if (photos.size() == 0) {
                                view.showEmptyView();
                            } else {
                                view.setItems(photos);
                                view.hideProgress();
                            }
                        }
                    }, error ->  {if (view != null) {
                        view.showErrorView();
                    }
        }));

    }

    @Override
    public void destroy() {
        view = null;
        mCompositeSubscription.unsubscribe();
    }

}
