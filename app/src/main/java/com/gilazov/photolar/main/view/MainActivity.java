package com.gilazov.photolar.main.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gilazov.photolar.R;
import com.gilazov.photolar.main.UINavigator;

public class MainActivity extends AppCompatActivity {

    private UINavigator uiNavigator;

    public void setUiNavigator(UINavigator uiNavigator) {
        this.uiNavigator = uiNavigator;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUiNavigator(new UINavigator(getSupportFragmentManager()));

        uiNavigator.showPhotos();

    }

}
