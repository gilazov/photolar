package com.gilazov.photolar.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.gilazov.photolar.R;
import com.gilazov.photolar.feature.photos_list.view.PhotosFragment;

public class UINavigator {

    private FragmentManager fragmentManager;

    public UINavigator(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void showPhotos() {

        if (fragmentManager != null) {

            PhotosFragment fragment = (PhotosFragment) fragmentManager.findFragmentByTag(Config.PHOTOS_FRAGMENT_TAG);

            if (fragment==null) {
                fragment = PhotosFragment.newInstance();
            }

            replaceMainFragment(fragment, Config.PHOTOS_FRAGMENT_TAG);

        }
    }

    public void replaceMainFragment(Fragment fragment, String fragmentTag){

        if (fragmentManager != null) {

            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, fragmentTag)
                    .commit();

        }

    }

}
