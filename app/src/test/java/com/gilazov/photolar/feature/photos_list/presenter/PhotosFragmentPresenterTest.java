package com.gilazov.photolar.feature.photos_list.presenter;

import com.gilazov.domain.model.Photo;
import com.gilazov.domain.usecase.GetPhotosUseCase;
import com.gilazov.photolar.base.view.MVPRecyclerFragmentView;
import com.gilazov.photolar.test.common.ImmediateSchedulersTestRule;
import com.gilazov.photolar.test.common.TestDataFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import rx.Observable;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by r on 10.11.16.

 */
@RunWith(MockitoJUnitRunner.class)
public class PhotosFragmentPresenterTest {

    @Rule
    public ImmediateSchedulersTestRule schedulersTestRule = new ImmediateSchedulersTestRule();

    @Mock
    MVPRecyclerFragmentView view;
    @Mock
    GetPhotosUseCase getPhotosUseCase;

    PhotosFragmentPresenter photosPresenter;

    @Before
    public void setup(){
        photosPresenter = new PhotosFragmentPresenter(view, getPhotosUseCase);

    }

    @After
    public void finish(){
        photosPresenter.destroy();
    }

    @Test
    public void loadDataReturnsPhotos(){
        List<Photo> populatedList = TestDataFactory.makeListPhotos(10);
        when(getPhotosUseCase.getPhotos())
                .thenReturn(Observable.just(populatedList));

        photosPresenter.loadData();

        verify(view).showProgress();
        verify(view).setItems(populatedList);
        verify(view, never()).showErrorView();
        verify(view, never()).showEmptyView();
        verify(view).hideProgress();

    }

    @Test
    public void loadDataReturnsEmptyList(){
        when(getPhotosUseCase.getPhotos())
                .thenReturn(Observable.just(Collections.emptyList()));

        photosPresenter.loadData();

        verify(view).showEmptyView();
        verify(view, never()).showErrorView();
        verify(view,never()).setItems(anyListOf(Photo.class));
    }

    @Test
    public void loadDataReturnsError(){

        when(getPhotosUseCase.getPhotos()).thenReturn(Observable.error(new Throwable()));

        photosPresenter.loadData();

        verify(view).showErrorView();
        verify(view, never()).showEmptyView();
        verify(view,never()).setItems(anyListOf(Photo.class));

    }
}