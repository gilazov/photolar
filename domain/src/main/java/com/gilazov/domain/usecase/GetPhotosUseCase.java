package com.gilazov.domain.usecase;

import com.gilazov.data.entity.PhotoDAO;
import com.gilazov.data.repository.PhotosRepository;
import com.gilazov.domain.model.Photo;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 24.10.16.
 */

public class GetPhotosUseCase {

    PhotosRepository repository;

    public GetPhotosUseCase(PhotosRepository repository) {
        this.repository = repository;
    }

    public Observable<List<Photo>> getPhotos(){
        return repository
                .getAll()
                .flatMap(Observable::from)
                .map(this::toPhoto)
                .toList();
    }

    private Photo toPhoto (PhotoDAO photoDAO){

        Photo photo = new Photo();
        if (photoDAO.getId() != null) {
            photo.setId(photoDAO.getId());
        }
        photo.setName(photoDAO.getName());
        photo.setUrl(photoDAO.getUrl());

        return photo;
    }
}
